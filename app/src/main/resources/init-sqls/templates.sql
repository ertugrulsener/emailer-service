INSERT INTO templates (id, template, description) VALUES
    (1, E'Sehr geehrte Damen und Herren,\n\nhiermit möchte ich, ${FULL_NAME}, weiterhin mein Interesse an der Mitgliedschaft bei der Kleingartenanlage Heimaterde bekunden.\nMeine Bewerber ID ist: ${CANDIDATE_ID}\n\nMit freundlichen Grüßen\n${FULL_NAME}', 'Template für die Interessenbekundung bei der Kleingartenanlage bei Heimaterde'),
    (2, E'Bewerber ${CANDIDATE_ID} - ${FULL_NAME}, Weiterhin Interesse an Kleingartenanlage Heimaterde', 'Template (Betreff) für die Interessenbekundung bei der Kleingartenanlage bei Heimaterde')
ON CONFLICT (id) DO UPDATE SET
        template = EXCLUDED.template,
        description = EXCLUDED.description;