INSERT INTO persons (id, first_name, middle_name, last_name, email, candidate_id) VALUES
    (1, 'Ertugrul', '', 'Sener', 'ertugrulsener@hotmail.de', '2023/59'),
    (2, 'Nuray', '', 'Cicek', 'nuray-cicek@live.de', '42/23'),
    (3, 'Tugce', 'Gülsüm', 'Kaya', 'tugceakman@live.de', '2023/58'),
    (4, '', '', '', 'anfrage@kga-heimaterde.de', '')
ON CONFLICT (id) DO UPDATE SET
        first_name = EXCLUDED.first_name,
        middle_name = EXCLUDED.middle_name,
        last_name = EXCLUDED.last_name,
        email = EXCLUDED.email,
        candidate_id = EXCLUDED.candidate_id;