INSERT INTO mails (id, sender_id, receiver_id, interval, time_to_send_next_mail, subject_template_id, subject_template_arguments, content_template_id, content_template_arguments) VALUES
    (1, 1, 4, '30d', null, 2, '{}', 1, '{}'),
    (2, 2, 4, '30d', null, 2, '{}', 1, '{}'),
    (3, 3, 4, '30d', null, 2, '{}', 1, '{}')
ON CONFLICT (id) DO UPDATE SET
        sender_id = EXCLUDED.sender_id,
        receiver_id = EXCLUDED.receiver_id,
        subject_template_id = EXCLUDED.subject_template_id,
        subject_template_arguments = EXCLUDED.subject_template_arguments,
        content_template_id = EXCLUDED.content_template_id,
        content_template_arguments = EXCLUDED.content_template_arguments;