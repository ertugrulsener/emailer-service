package com.nevertell.emailer.configurations;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.nevertell.emailer.notifications.PushNotifier;

import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
public class PushNotifierConfiguration {
    private final EmailerNotificationConfiguration emailerNotificationConfiguration;

    @Bean
    @ConditionalOnMissingBean
    public PushNotifier pushNotifier() {
        return new PushNotifier(emailerNotificationConfiguration.getUsername(),
                emailerNotificationConfiguration.getPassword(),
                emailerNotificationConfiguration.getAppPackage(),
                emailerNotificationConfiguration.getApiKey());
    }
}
