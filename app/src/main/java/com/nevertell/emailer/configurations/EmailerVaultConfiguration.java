package com.nevertell.emailer.configurations;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

@Configuration
@ConfigurationProperties("emailer.vault")
@Getter
@Setter
public class EmailerVaultConfiguration {
    private String host;

    private int port;

    private String scheme = "http";

    private String token;
}
