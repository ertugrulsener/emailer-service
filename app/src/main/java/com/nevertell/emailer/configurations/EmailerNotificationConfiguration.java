package com.nevertell.emailer.configurations;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

@Configuration
@ConfigurationProperties("emailer.notification")
@Getter
@Setter
public class EmailerNotificationConfiguration {
    private boolean enabled;

    private String username = null;

    private String password = null;

    private String appPackage = null;

    private String apiKey = null;
}
