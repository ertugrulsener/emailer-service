package com.nevertell.emailer.configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.vault.authentication.ClientAuthentication;
import org.springframework.vault.authentication.TokenAuthentication;
import org.springframework.vault.client.VaultEndpoint;
import org.springframework.vault.config.AbstractVaultConfiguration;

import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
class VaultConfiguration extends AbstractVaultConfiguration {
    private final EmailerVaultConfiguration emailerVaultConfiguration;

    @Override
    public VaultEndpoint vaultEndpoint() {
        VaultEndpoint vaultEndpoint = VaultEndpoint.create(emailerVaultConfiguration.getHost(),
                emailerVaultConfiguration.getPort());
        vaultEndpoint.setScheme(emailerVaultConfiguration.getScheme());
        return vaultEndpoint;
    }

    @Override
    public ClientAuthentication clientAuthentication() {
        return new TokenAuthentication(emailerVaultConfiguration.getToken());
    }
}