package com.nevertell.emailer.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nevertell.emailer.database.model.TemplateEntity;

public interface TemplateRepository extends JpaRepository<TemplateEntity, Long> {
}
