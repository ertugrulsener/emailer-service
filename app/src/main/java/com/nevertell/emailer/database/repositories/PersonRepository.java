package com.nevertell.emailer.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nevertell.emailer.database.model.PersonEntity;

public interface PersonRepository extends JpaRepository<PersonEntity, Long> {
}
