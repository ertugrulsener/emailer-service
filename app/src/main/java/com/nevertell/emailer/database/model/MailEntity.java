package com.nevertell.emailer.database.model;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.annotations.Type;

import io.hypersistence.utils.hibernate.type.json.JsonType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity(name = "Mails")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class MailEntity {
    @Id
    @EqualsAndHashCode.Include
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private PersonEntity sender;

    @ManyToOne(fetch = FetchType.LAZY)
    private PersonEntity receiver;

    private LocalDateTime timeToSendNextMail;

    private Duration interval;

    @ManyToOne(fetch = FetchType.LAZY)
    private TemplateEntity subjectTemplate;

    @Type(JsonType.class)
    @Column(columnDefinition = "jsonb")
    private Map<String, String> subjectTemplateArguments = new HashMap<>();

    @ManyToOne(fetch = FetchType.LAZY)
    private TemplateEntity contentTemplate;

    @Type(JsonType.class)
    @Column(columnDefinition = "jsonb")
    private Map<String, String> contentTemplateArguments = new HashMap<>();

    @Transient
    public String getSubjectTemplateText() {
        return subjectTemplate.getTemplate();
    }

    @Transient
    public String getContentTemplateAsText() {
        return contentTemplate.getTemplate();
    }
}
