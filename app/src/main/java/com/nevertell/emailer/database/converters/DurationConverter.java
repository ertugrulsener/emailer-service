package com.nevertell.emailer.database.converters;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

import org.springframework.boot.convert.DurationStyle;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import lombok.NonNull;

@Converter(autoApply = true)
public class DurationConverter implements AttributeConverter<Duration, String> {
    @Override
    public String convertToDatabaseColumn(@NonNull Duration attribute) {
        return DurationStyle.SIMPLE.print(attribute, ChronoUnit.MINUTES);
    }

    @Override
    public Duration convertToEntityAttribute(@NonNull String durationAsString) {
        return DurationStyle.detectAndParse(durationAsString);
    }
}
