package com.nevertell.emailer.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.nevertell.emailer.database.model.MailEntity;

public interface MailRepository extends JpaRepository<MailEntity, Long> {
    @Query("SELECT m FROM Mails m WHERE m.timeToSendNextMail = null OR m.timeToSendNextMail < CURRENT_TIMESTAMP")
    List<MailEntity> getAllExpiredMails();
}
