package com.nevertell.emailer.services;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.nevertell.emailer.database.model.PersonEntity;

import lombok.NonNull;

@Service
public class PersonService {
    public String getPersonsFullName(@NonNull PersonEntity person) {
        return Stream.of(person.getFirstName(), person.getMiddleName(), person.getLastName())
                .filter(StringUtils::isNotBlank)
                .collect(Collectors.joining(" "));
    }
}
