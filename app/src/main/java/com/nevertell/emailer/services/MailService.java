package com.nevertell.emailer.services;

import com.nevertell.emailer.configurations.EmailerNotificationConfiguration;
import com.nevertell.emailer.database.model.MailEntity;
import com.nevertell.emailer.database.model.PersonEntity;
import com.nevertell.emailer.notifications.PushNotifier;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.text.StringSubstitutor;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class MailService {
    private static final String TEMPLATE_PREFIX = "${";

    private static final String TEMPLATE_POSTFIX = "}";

    private final EmailerNotificationConfiguration emailerNotificationConfiguration;

    private final JavaMailSenderImpl emailSender;

    private final PersonService personService;

    private final VaultService vaultService;

    private final Environment environment;

    private final PushNotifier pushNotifier;

    private String substituteString(String template, Map<String, String> arguments) {
        return StringSubstitutor.replace(template, arguments, TEMPLATE_PREFIX, TEMPLATE_POSTFIX);
    }

    private String resolveSenderInformation(PersonEntity sender, String template) {
        var senderArguments = Map.of("FULL_NAME",
                personService.getPersonsFullName(sender),
                "CANDIDATE_ID",
                sender.getCandidateId(),
                "EMAIL",
                sender.getEmail());
        return substituteString(template, senderArguments);
    }

    void sendMail(PersonEntity sender, PersonEntity receiver, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(sender.getEmail());
        message.setTo(receiver.getEmail());
        message.setSubject(subject);
        message.setText(text);

        log.info("Sending mail with subject '%s' to: '%s'".formatted(subject, sender.getEmail()));
        log.trace("%s".formatted(message));

        if (Arrays.asList(environment.getActiveProfiles()).contains("prod")) {
            emailSender.setDefaultEncoding(StandardCharsets.UTF_8.name());
            emailSender.setUsername(sender.getEmail());
            emailSender.setPassword(vaultService.getMailPassword(sender.getEmail()));
            emailSender.send(message);
        }
    }

    public void sendMail(MailEntity expiredMail) {
        String preResolvedSubject = resolveSenderInformation(expiredMail.getSender(),
                expiredMail.getSubjectTemplateText());
        String resolvedSubject = substituteString(preResolvedSubject, expiredMail.getSubjectTemplateArguments());

        String preResolvedContent = resolveSenderInformation(expiredMail.getSender(),
                expiredMail.getContentTemplateAsText());
        String resolvedContent = substituteString(preResolvedContent, expiredMail.getContentTemplateArguments());

        sendMail(expiredMail.getSender(), expiredMail.getReceiver(), resolvedSubject, resolvedContent);

        if (emailerNotificationConfiguration.isEnabled()) {
            sendSuccessNotification("Emailer: Mail sent from [%s] to [%s] - Subject [%s]".formatted(expiredMail.getSender()
                    .getEmail(), expiredMail.getReceiver().getEmail(), resolvedSubject));
        }
    }

    void sendSuccessNotification(String message) {
        pushNotifier.sendNotification(message);
    }
}
