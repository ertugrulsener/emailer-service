package com.nevertell.emailer.services;

import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.vault.core.VaultKeyValueOperations;
import org.springframework.vault.core.VaultKeyValueOperationsSupport.KeyValueBackend;
import org.springframework.vault.core.VaultTemplate;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class VaultService {
    private final VaultTemplate vaultTemplate;

    public String getMailPassword(String email) {
        VaultKeyValueOperations basePath = vaultTemplate.opsForKeyValue("cubbyhole", KeyValueBackend.KV_1);
        var emailPath = basePath.get("emails", (Class<Map<String, String>>) (Class) Map.class);

        if (emailPath == null || emailPath.getData() == null || emailPath.getData().isEmpty()) {
            throw new IllegalArgumentException("Could not find email path or data");
        }

        return emailPath.getData().get(email);
    }
}
