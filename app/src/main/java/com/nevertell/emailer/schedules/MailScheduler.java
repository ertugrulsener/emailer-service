package com.nevertell.emailer.schedules;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.mail.MailAuthenticationException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.nevertell.emailer.database.model.MailEntity;
import com.nevertell.emailer.database.repositories.MailRepository;
import com.nevertell.emailer.services.MailService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@RequiredArgsConstructor
public class MailScheduler {
    private final MailRepository mailRepository;

    private final MailService mailService;

    @Scheduled(fixedRate = 60, initialDelay = 5, timeUnit = TimeUnit.SECONDS)
    @Transactional
    public void checkIfMailsNeedToBeSent() {
        log.debug("Checking if mails need to be sent");

        List<MailEntity> expiredMails = mailRepository.getAllExpiredMails();

        for (MailEntity expiredMail : expiredMails) {
            log.info("Sending mail from %s to %s".formatted(expiredMail.getSender().getEmail(),
                    expiredMail.getReceiver().getEmail()));

            try {
                mailService.sendMail(expiredMail);
            } catch (MailAuthenticationException mailAuthenticationException) {
                log.error("Authentication failed for email %s with message: %s".formatted(expiredMail.getSender(),
                        mailAuthenticationException.getMessage()));
                continue;
            }

            LocalDateTime timeToSendNextMail = LocalDateTime.now().plus(expiredMail.getInterval());
            expiredMail.setTimeToSendNextMail(timeToSendNextMail);
        }
    }
}
