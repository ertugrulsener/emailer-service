package com.nevertell.emailer.notifications.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public record GetDeviceResponse(String id, String title, String model, @JsonProperty("nice_model") String niceModel,
                                String image) {
}
