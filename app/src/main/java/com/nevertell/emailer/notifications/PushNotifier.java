package com.nevertell.emailer.notifications;

import java.util.Base64;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import com.nevertell.emailer.notifications.dtos.GetDeviceResponse;
import com.nevertell.emailer.notifications.dtos.LoginRequest;
import com.nevertell.emailer.notifications.dtos.LoginResponse;
import com.nevertell.emailer.notifications.dtos.SendNotificationRequest;
import com.nevertell.emailer.notifications.dtos.SendNotificationResponse;

import lombok.Generated;

// TODO: Move this class to an own project, it's not really related to emailer
@Generated
public class PushNotifier {
    private static final String PUSHNOTIFIER_V2_API = "https://api.pushnotifier.de/v2";

    private final String username;

    private final String password;

    private final String appToken;

    private final String appPackage;

    private final String apiKey;

    private final RestTemplate restTemplate;

    public PushNotifier(String username, String password, String appPackage, String apiKey) {
        this.username = username;
        this.password = password;
        this.appPackage = appPackage;
        this.apiKey = apiKey;

        this.restTemplate = new RestTemplate();
        this.restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory(PUSHNOTIFIER_V2_API));

        var loginResponse = login();
        this.appToken = loginResponse.appToken();
    }

    private LoginResponse login() {
        LoginRequest loginRequest = new LoginRequest(username, password);

        return restTemplate.exchange("/user/login",
                HttpMethod.POST,
                new HttpEntity<>(loginRequest, httpHeaders()),
                LoginResponse.class).getBody();
    }

    private List<GetDeviceResponse> getDevices() {
        return restTemplate.exchange("/devices",
                HttpMethod.GET,
                new HttpEntity<>(httpHeaders()),
                new ParameterizedTypeReference<List<GetDeviceResponse>>() {
                }).getBody();
    }

    public SendNotificationResponse sendNotification(String content) {
        List<String> deviceIds = getDevices().stream().map(GetDeviceResponse::id).toList();
        SendNotificationRequest sendNotificationRequest = new SendNotificationRequest(deviceIds, content);

        return restTemplate.exchange("/notifications/text",
                HttpMethod.PUT,
                new HttpEntity<>(sendNotificationRequest, httpHeaders()),
                SendNotificationResponse.class).getBody();
    }

    private HttpHeaders httpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Basic " + getBasicAuthHeader());

        if (appToken != null) {
            httpHeaders.add("X-AppToken", appToken);
        }

        httpHeaders.setAccept(List.of(MediaType.APPLICATION_JSON));
        return httpHeaders;
    }

    private String getBasicAuthHeader() {
        String credentials = "%s:%s".formatted(appPackage, apiKey);
        return Base64.getEncoder().encodeToString(credentials.getBytes());
    }
}
