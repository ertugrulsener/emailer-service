package com.nevertell.emailer.notifications.dtos;

public record LoginRequest(String username, String password) {
}
