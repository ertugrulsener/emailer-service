package com.nevertell.emailer.notifications.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public record LoginResponse(String username, String avatar, @JsonProperty("app_token") String appToken,
                            @JsonProperty("expires_at") int expiresAt) {
}
