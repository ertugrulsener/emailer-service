package com.nevertell.emailer.notifications.dtos;

import java.util.List;

public record SendNotificationRequest(List<String> devices, String content) {
}
