package com.nevertell.emailer.notifications.dtos;

import java.util.List;

public record SendNotificationResponse(List<Device> success, List<String> error) {
}
