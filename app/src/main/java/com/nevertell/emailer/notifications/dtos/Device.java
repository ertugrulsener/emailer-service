package com.nevertell.emailer.notifications.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Device {
    @JsonProperty("device_id")
    private String deviceId;
}