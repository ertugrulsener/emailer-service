package com.nevertell.emailer.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import com.nevertell.emailer.database.model.PersonEntity;

class PersonServiceTest {
    private final PersonService personService = new PersonService();

    @Test
    void fullNameShouldBeEmptyIfNotSet() {
        assertEquals("", personService.getPersonsFullName(new PersonEntity()));
    }

    @Test
    void shouldThrowIfPersonIsNull() {
        assertThrows(NullPointerException.class, () -> personService.getPersonsFullName(null));
    }

    @Test
    void shouldReturnPersonFullName() {
        // given
        PersonEntity person = new PersonEntity();
        person.setFirstName("First");
        person.setMiddleName("Middle");
        person.setLastName("Second");

        // when && then
        assertEquals("First Middle Second", personService.getPersonsFullName(person));
    }

    @Test
    void shouldReturnPersonFullNameIfOnlyFirstAndLastNameIsSet() {
        // given
        PersonEntity person = new PersonEntity();
        person.setFirstName("First");
        person.setLastName("Second");

        // when && then
        assertEquals("First Second", personService.getPersonsFullName(person));
    }

    @Test
    void shouldReturnPersonFullNameIfOnlyFirstNameIsSet() {
        // given
        PersonEntity person = new PersonEntity();
        person.setFirstName("First");

        // when && then
        assertEquals("First", personService.getPersonsFullName(person));
    }
}
