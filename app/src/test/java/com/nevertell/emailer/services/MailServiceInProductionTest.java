package com.nevertell.emailer.services;

import static com.nevertell.emailer.services.MailServiceTest.getScenario;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.test.context.ActiveProfiles;

import com.nevertell.emailer.configurations.PushNotifierInTestConfiguration;
import com.nevertell.emailer.services.MailServiceTest.Scenario;
import com.nevertell.emailer.testcontainers.EnableTestcontainers;

@SpringBootTest(properties = "emailer.notification.enabled=false")
@Import(PushNotifierInTestConfiguration.class)
@ActiveProfiles("prod")
@EnableTestcontainers
class MailServiceInProductionTest {
    @SpyBean
    private MailService mailService;

    @MockBean
    private JavaMailSenderImpl emailSender;

    @Test
    void mailShouldOnlyBeSentInProduction() {
        // given
        Scenario result = getScenario();

        // when
        mailService.sendMail(result.mail());

        // then
        verify(emailSender).send(any(SimpleMailMessage.class));
    }
}
