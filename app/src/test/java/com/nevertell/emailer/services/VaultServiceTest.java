package com.nevertell.emailer.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.vault.core.VaultKeyValueOperations;
import org.springframework.vault.core.VaultTemplate;
import org.springframework.vault.support.VaultResponseSupport;

@ContextConfiguration(classes = {VaultService.class})
@ExtendWith(SpringExtension.class)
class VaultServiceTest {
    @Autowired
    private VaultService vaultService;

    @MockBean
    private VaultTemplate vaultTemplate;

    @Test
    void testGetMailPasswordWithEmptyVault() {
        // given
        when(vaultTemplate.opsForKeyValue(Mockito.any(), Mockito.any())).thenReturn(mock(VaultKeyValueOperations.class));

        // when && then
        var throwable = assertThrows(IllegalArgumentException.class, () -> vaultService.getMailPassword("jane.doe@example.org"));
        assertEquals("Could not find email path or data", throwable.getMessage());

        verify(vaultTemplate).opsForKeyValue(Mockito.any(), Mockito.any());
    }

    @Test
    void testGetMailPasswordWithVaultButNoData() {
        // given
        var vaultKeyValueOperations = mock(VaultKeyValueOperations.class);
        when(vaultTemplate.opsForKeyValue(Mockito.any(), Mockito.any())).thenReturn(vaultKeyValueOperations);

        // when
        VaultResponseSupport<Map<Object, Object>> vaultResponseSupport = new VaultResponseSupport<>();
        vaultResponseSupport.setData(null);

        when(vaultKeyValueOperations.get(Mockito.any(), Mockito.<Class<Map<Object, Object>>>any())).thenReturn(vaultResponseSupport);

        // then
        var throwable = assertThrows(IllegalArgumentException.class, () -> vaultService.getMailPassword("jane.doe@example.org"));
        assertEquals("Could not find email path or data", throwable.getMessage());

        verify(vaultTemplate).opsForKeyValue(Mockito.any(), Mockito.any());
    }

    @Test
    void testGetMailPasswordWithVaultButEmptyData() {
        // given
        var vaultKeyValueOperations = mock(VaultKeyValueOperations.class);
        when(vaultTemplate.opsForKeyValue(Mockito.any(), Mockito.any())).thenReturn(vaultKeyValueOperations);

        // when
        VaultResponseSupport<Map<Object, Object>> vaultResponseSupport = new VaultResponseSupport<>();
        vaultResponseSupport.setData(Collections.emptyMap());

        when(vaultKeyValueOperations.get(Mockito.any(), Mockito.<Class<Map<Object, Object>>>any())).thenReturn(vaultResponseSupport);

        // then
        var throwable = assertThrows(IllegalArgumentException.class, () -> vaultService.getMailPassword("jane.doe@example.org"));
        assertEquals("Could not find email path or data", throwable.getMessage());

        verify(vaultTemplate).opsForKeyValue(Mockito.any(), Mockito.any());
    }

    /**
     * Method under test: {@link VaultService#getMailPassword(String)}
     */
    @Test
    void retrieveMailPassword() {
        // given
        final String EMAIL = "jane.doe@example.org";
        final String EXPECTED_PASSWORD = "myPassword";

        VaultResponseSupport<Map<Object, Object>> vaultResponseSupport = new VaultResponseSupport<>();
        vaultResponseSupport.setAuth(new HashMap<>());
        vaultResponseSupport.setData(Map.of(EMAIL, EXPECTED_PASSWORD));
        vaultResponseSupport.setLeaseDuration(1L);
        vaultResponseSupport.setLeaseId("42");
        vaultResponseSupport.setMetadata(new HashMap<>());
        vaultResponseSupport.setRenewable(true);
        vaultResponseSupport.setRequestId("42");
        vaultResponseSupport.setWarnings(new ArrayList<>());
        vaultResponseSupport.setWrapInfo(new HashMap<>());

        VaultKeyValueOperations vaultKeyValueOperations = mock(VaultKeyValueOperations.class);
        when(vaultKeyValueOperations.get(Mockito.any(), Mockito.<Class<Map<Object, Object>>>any())).thenReturn(vaultResponseSupport);
        when(vaultTemplate.opsForKeyValue(Mockito.any(), Mockito.any())).thenReturn(
                vaultKeyValueOperations);

        // when
        String actualMailPassword = vaultService.getMailPassword(EMAIL);

        // then
        verify(vaultKeyValueOperations).get(Mockito.any(), Mockito.any());
        verify(vaultTemplate).opsForKeyValue(Mockito.any(), Mockito.any());

        assertNotNull(actualMailPassword);
        assertEquals(EXPECTED_PASSWORD, actualMailPassword);
    }
}
