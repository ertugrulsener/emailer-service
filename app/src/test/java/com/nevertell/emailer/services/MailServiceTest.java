package com.nevertell.emailer.services;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Map;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;

import com.nevertell.emailer.configurations.EmailerNotificationConfiguration;
import com.nevertell.emailer.configurations.PushNotifierInTestConfiguration;
import com.nevertell.emailer.database.model.MailEntity;
import com.nevertell.emailer.database.model.PersonEntity;
import com.nevertell.emailer.database.model.TemplateEntity;
import com.nevertell.emailer.testcontainers.EnableTestcontainers;

@SpringBootTest
@Import(PushNotifierInTestConfiguration.class)
@EnableTestcontainers
class MailServiceTest {
    @SpyBean
    private MailService mailService;

    @MockBean
    private EmailerNotificationConfiguration emailerNotificationConfiguration;

    record Scenario(PersonEntity sender, PersonEntity receiver, MailEntity mail) {
    }

    @Test
    void testSubstitutionOfSubject() {
        // given
        Scenario result = getScenario();

        // when
        mailService.sendMail(result.mail());

        // then
        verify(mailService).sendMail(eq(result.sender()), eq(result.receiver()), eq("Hallo Jane Doe, dies ist eine Testmail von jane.doe@gmail.com! \n"
                + "Dies ist ein Beispieltext!"), any(String.class));
    }

    static Scenario getScenario() {
        TemplateEntity subjectTemplate = new TemplateEntity();
        subjectTemplate.setTemplate("Hallo ${FULL_NAME}, dies ist eine Testmail von ${EMAIL}! ${TEXT}");

        TemplateEntity contentTemplate = new TemplateEntity();
        contentTemplate.setTemplate("Das ist der Inhalt");

        PersonEntity sender = new PersonEntity();
        sender.setCandidateId(UUID.randomUUID().toString());
        sender.setFirstName("Jane");
        sender.setLastName("Doe");
        sender.setEmail("jane.doe@gmail.com");

        PersonEntity receiver = new PersonEntity();
        receiver.setCandidateId(UUID.randomUUID().toString());
        receiver.setFirstName("Receiver");
        receiver.setLastName("Joe");
        receiver.setEmail("receiver.joe@gmail.com");

        MailEntity mail = new MailEntity();
        mail.setSender(sender);
        mail.setReceiver(receiver);
        mail.setSubjectTemplate(subjectTemplate);
        mail.setSubjectTemplateArguments(Map.of("TEXT", "\nDies ist ein Beispieltext!"));
        mail.setContentTemplate(contentTemplate);
        mail.setContentTemplateArguments(Collections.emptyMap());
        return new Scenario(sender, receiver, mail);
    }

    @Test
    void notificationShouldBeSentIfEnabled() {
        // given
        Scenario result = getScenario();

        // when
        when(emailerNotificationConfiguration.isEnabled()).thenReturn(true);
        mailService.sendMail(result.mail());

        // then
        verify(mailService).sendSuccessNotification(any(String.class));
    }
}
