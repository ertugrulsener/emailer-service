package com.nevertell.emailer.schedules;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.nevertell.emailer.database.model.MailEntity;
import com.nevertell.emailer.database.model.PersonEntity;
import com.nevertell.emailer.database.repositories.MailRepository;
import com.nevertell.emailer.services.MailService;
import java.time.Duration;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mail.MailAuthenticationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {MailScheduler.class})
@ExtendWith(SpringExtension.class)
class MailSchedulerTest {
    @Autowired
    private MailScheduler mailScheduler;

    @MockBean
    private MailRepository mailRepository;

    @MockBean
    private MailService mailService;

    @Test
    void timeToBeSentShouldBeUpdatedAfterSendingMail() {
        // given
        PersonEntity sender = new PersonEntity();
        sender.setEmail("sender@mail.de");

        PersonEntity receiver = new PersonEntity();
        sender.setEmail("receiver@mail.de");

        MailEntity EXPIRED_MAIL_ENTITY = spy(new MailEntity());
        EXPIRED_MAIL_ENTITY.setSender(sender);
        EXPIRED_MAIL_ENTITY.setInterval(Duration.ofMinutes(5));
        EXPIRED_MAIL_ENTITY.setReceiver(receiver);

        when(mailRepository.getAllExpiredMails()).thenReturn(List.of(EXPIRED_MAIL_ENTITY));

        // Act
        mailScheduler.checkIfMailsNeedToBeSent();

        // Assert that nothing has changed
        verify(mailRepository).getAllExpiredMails();
        verify(mailService).sendMail(EXPIRED_MAIL_ENTITY);
        verify(EXPIRED_MAIL_ENTITY).setTimeToSendNextMail(any());
    }

    @Test
    void timeToBeSentShouldNotBeUpdatedIfMailAuthenticationFails() {
        // given
        PersonEntity sender = new PersonEntity();
        sender.setEmail("sender@mail.de");

        PersonEntity receiver = new PersonEntity();
        sender.setEmail("receiver@mail.de");

        MailEntity EXPIRED_MAIL_ENTITY = spy(new MailEntity());
        EXPIRED_MAIL_ENTITY.setSender(sender);
        EXPIRED_MAIL_ENTITY.setInterval(Duration.ofMinutes(5));
        EXPIRED_MAIL_ENTITY.setReceiver(receiver);

        when(mailRepository.getAllExpiredMails()).thenReturn(List.of(EXPIRED_MAIL_ENTITY));
        doThrow(new MailAuthenticationException("fail")).when(mailService).sendMail(EXPIRED_MAIL_ENTITY);

        // Act
        mailScheduler.checkIfMailsNeedToBeSent();

        // Assert that nothing has changed
        verify(mailRepository).getAllExpiredMails();
        verify(mailService).sendMail(EXPIRED_MAIL_ENTITY);
        verify(EXPIRED_MAIL_ENTITY, times(0)).setTimeToSendNextMail(any());
    }
}
