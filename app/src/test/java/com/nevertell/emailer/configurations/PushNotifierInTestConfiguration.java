package com.nevertell.emailer.configurations;

import static org.mockito.Mockito.mock;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

import com.nevertell.emailer.notifications.PushNotifier;

@TestConfiguration
public class PushNotifierInTestConfiguration {
    @Bean
    public PushNotifier pushNotifier() {
        return mock(PushNotifier.class);
    }
}
