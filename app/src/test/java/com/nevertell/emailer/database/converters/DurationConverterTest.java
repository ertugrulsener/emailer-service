package com.nevertell.emailer.database.converters;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

import org.junit.jupiter.api.Test;
import org.springframework.boot.convert.DurationStyle;

class DurationConverterTest {
    private final DurationConverter durationConverter = new DurationConverter();

    @Test
    void convertingToEntityAttribute() {
        // given
        int EXPECTED_DURATION_IN_MINUTES = 50;
        String durationString = Duration.ofMinutes(EXPECTED_DURATION_IN_MINUTES).toString();

        // when
        Duration result = durationConverter.convertToEntityAttribute(durationString);

        // then
        assertEquals(EXPECTED_DURATION_IN_MINUTES, result.toMinutes());
    }

    @Test
    void convertingToEntityAttributeWithNullThrowsNPE() {
        assertThrows(NullPointerException.class, () -> durationConverter.convertToEntityAttribute(null));
    }

    @Test
    void convertingToDatabaseColumnType() {
        // given
        Duration DURATION = Duration.ofMinutes(50);
        String EXPECTED_DURATION_STRING = DurationStyle.SIMPLE.print(DURATION, ChronoUnit.MINUTES);

        // when
        String result = durationConverter.convertToDatabaseColumn(DURATION);

        // then
        assertEquals(EXPECTED_DURATION_STRING, result);
    }

    @Test
    void convertingToDatabaseColumnTypeWithNullThrowsNPE() {
        assertThrows(NullPointerException.class, () -> durationConverter.convertToDatabaseColumn(null));
    }
}
