# E-Mailer

This service ill send e-mails to a client on a specified interval based on user specified templates.
If required, it wil also notify the administrator (or any other person) by sending push
notifications to his / her phone.

# Components

- Postgres
- PgAdmin

# Prerequisites

- Docker / Podman